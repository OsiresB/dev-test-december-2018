// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

var animal = { Nome: "cao", Cor: "preto", Tamanho:"grande" }
var pessoa = {Nome: "Osires", Profissão: "Estudante", Cidade: "São Carlos"}

document.write(Object.keys(animal), '<br>')
document.write(Object.keys(pessoa), '<br>', '<br>')

function removeProperty(obj, prop) {
  if (obj.hasOwnProperty(prop) === false  ) {  
		return false	
  	}else{		
  		delete obj[prop]
  		return  true
  	}
}

document.write(removeProperty(animal, 'comida_tipo'), '<br>',  Object.keys(animal), '<br>')
document.write(removeProperty(pessoa, 'Profissão'), '<br>', Object.keys(pessoa), '<br>')