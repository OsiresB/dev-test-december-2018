<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        $tempor = $files;
		$result0 = array();
		$result1 = array();
		$resultado = array();

		foreach ($files as $key => $value) {
			foreach ($tempor as $key1 => $value1) {
				if( ($value == $value1) && ($key != $key1) ){
					$result0[$value] = $key1. ', ' .$key;
				}
				else{
					$result1[$value] = $key;
				}
			}

		$resultado = $result0+$result1 ;
		} 

        return $resultado;
    }
}

$resultad = FileOwners::groupByOwners($file = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

));

print_r($resultad);