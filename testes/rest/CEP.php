<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}

Documentação:
https://postmon.com.br/

*/

class CEP
{
    public static function getAddressByCep($cep)
    {
        $DadosAddress = "";
		$cepNr = preg_replace("[^0-9]", "", $cep);    //converter cep pra ficar só numerico
		$url = "http://viacep.com.br/ws/$cepNr/xml/";  // endereço onde vai se conectar pra pegar dados de endereço considerando o $cep
		$AddressCep = simplexml_load_file($url);

		// Salvar as Dados do Endereço
		$DadosAddress = ("Dadod do Endereço Procurado: <br> --------------------------- <br>". 
			"Bairo: " .$AddressCep->bairro. "<br>Cidade: " .$AddressCep->localidade.	"<br>Logradouro: "  .$AddressCep->logradouro. 
			"<br>Codigo_ibge:" .$AddressCep->ibge. "<br>Estado: " .$AddressCep->uf. "<br> CEP: " .$AddressCep->cep.
			"<br> Guia: " .$AddressCep->gia. "<br>Complemento: " .$AddressCep->complemento. "<br>Unidade: " .$AddressCep->unidade);

		return $DadosAddress;    // discumenta caso quer ver resultado pelo echo(), e comenta linha seguinte.
		//return $AddressCep;   // discumenta caso quer ver resultado pelo var_dump(), e comenta linha anterior.
    }
}  // fim da class CEP

$cep_procurado = "13566-447";
//var_dump(CEP::getAddressByCep($cep_procurado));
// echo "<br>"," ================================================================", "<br>";
echo (CEP::getAddressByCep($cep_procurado));